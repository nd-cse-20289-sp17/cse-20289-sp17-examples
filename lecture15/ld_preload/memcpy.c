/* https://bugzilla.redhat.com/show_bug.cgi?id=638477#c38 */

#include <sys/types.h>
#include <stdio.h>

void *memcpy(void *dst, const void *src, size_t size) {
    void *orig = dst;
    puts("Linus' memcpy hack");

    asm volatile("rep ; movsq"
	    :"=D" (dst), "=S" (src)
	    :"0" (dst), "1" (src), "c" (size >> 3)
	    :"memory");
    asm volatile("rep ; movsb"
	    :"=D" (dst), "=S" (src)
	    :"0" (dst), "1" (src), "c" (size & 7)
	    :"memory");
    return orig;
}
