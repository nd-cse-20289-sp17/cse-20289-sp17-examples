#define ARRAY_SIZE  (1<<15)
#define ITEM_MAX    (1<<8)
#define LOOP_MAX    (1<<16)

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
    int64_t data[ARRAY_SIZE];

    for (int i = 0; i < ARRAY_SIZE; i++) {
    	data[i] = rand() % ITEM_MAX;
    }

    int64_t sum = 0;

    for (int i = 0; i < ARRAY_SIZE; i++) {
	if (data[i] >= (ITEM_MAX/2)) {
	    for (int l = 0; l < LOOP_MAX; l++) {
		sum += data[i];
	    }
	}
    }

    printf("sum is %ld\n", sum);

    return 0;
}
