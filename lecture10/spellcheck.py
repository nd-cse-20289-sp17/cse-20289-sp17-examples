#!/usr/bin/env python2.7

# SPELLCHECK.PY #################################################################

import sys

# VARIANT 1: List

'''
WORDS = []
for word in open('/usr/share/dict/words'):
    WORDS.append(word.rstrip())

for line in sys.stdin:
    for word in line.split():
        word = word.lower()
        if word not in WORDS:
            print '{} is misspelled'.format(word)
'''

# VARIANT 2: Set

'''
WORDS = set()
for word in open('/usr/share/dict/words'):
    WORDS.add(word.rstrip())

for line in sys.stdin:
    for word in line.split():
        word = word.lower()
        if word not in WORDS:
            print '{} is misspelled'.format(word)
'''

# VARIANT 3: Dictionary

WORDS = {}
for word in open('/usr/share/dict/words'):
    WORDS[word.rstrip()] = True

for line in sys.stdin:
    for word in line.split():
        word = word.lower()
        if word not in WORDS:
            print '{} is misspelled'.format(word)
