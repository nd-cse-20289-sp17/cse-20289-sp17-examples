#!/bin/sh

if [ "$#" -eq 0 ]; then
    echo "Usage: $(basename $0) host..."
    exit 1
fi

EXIT=0
for host in $@; do
    if ping -c 5 $host -W 2 2>&1 > /dev/null; then
	echo "$host is UP!"
    else
	echo "$host is DOWN!"
	EXIT=1
    fi
done

exit $EXIT
