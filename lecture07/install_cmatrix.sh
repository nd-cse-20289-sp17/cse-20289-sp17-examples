#!/bin/sh
# Download and install cmatrix

URL=http://www.asty.org/cmatrix/dist/cmatrix-1.2a.tar.gz
PREFIX=${PREFIX:-/tmp/pub}

wget $URL
tar xvf $(basename $URL)

cd cmatrix-1.2a
./configure --prefix $PREFIX
make
make install
